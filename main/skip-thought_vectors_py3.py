'''
Created on Oct 31, 2016

@author: amita
'''
import file_utilities
import os,sys
projectdir=file_utilities.PROJECT_DIR
skipthoughtdir=os.path.join(projectdir,"main","skip-thoughts-master_py3")
import sys
import pandas as pd
import numpy as np
sys.path.insert(0, skipthoughtdir)
import eval_trec
import skipthoughts
import eval_sick
model = skipthoughts.load_model()
def getskipthought(df,datacolumn):
    sentences_column=df[datacolumn1].values
    skipthoughtsvectors=encod_liststrings(sentences_column)
    return(skipthoughtsvectors)


def getskipthoughtpair(df,datacolumn1,datacolumn2):
    sentences_column1=df[datacolumn1].values
    skipthoughtvectors_1=getskipthought(df,sentences_column1)
    
    sentences_column2=df[datacolumn2].values
    skipthoughtvectors_2=getskipthought(df,sentences_column2)
    return(skipthoughtvectors_1,skipthoughtvectors_2)

def pairtofeature(pairvector):
        number= pairvector.shape[1]     
        columnnames=["skipthought_"+ str(i) for i in range(0,number)] 
        df=pd.DataFrame(pairvector,columns=columnnames) 
        return df
    
def skipvectorstofeature_df(skipthoughtvectors_1,skipthoughtvectors_2):
        number1= skipthoughtvectors_1.shape[1]     
        columnnames_1=["skipthought_sent1_"+ str(i) for i in range(0,number1)] 
        df1=pd.DataFrame(skipthoughtvectors_1,columns=columnnames_1) 
        
        number2= skipthoughtvectors_1.shape[1]      
        columnnames_2=["skipthought_sent2_"+ str(i) for i in range(0,number2)]  
        df2=pd.DataFrame(skipthoughtvectors_2,columns=columnnames_2)
        mergeddf=df1.join(df2)
        return mergeddf
    
def encod_liststrings(X):
    """
    X:list of strings to encode using skipthought
    """
   
    vectors = skipthoughts.encode(model, X,verbose=False)
    return vectors

def  testskipthought(inputcsv,outcsv_features,datacolumn1,datacolumn2):
     df=pd.read_csv(inputcsv)
     skipthoughtvectors_1,skipthoughtvectors_2 =getskipthoughtpair(df,datacolumn1,datacolumn2)
     print('Computing feature combinations...')
     pairvector= np.c_[np.abs(skipthoughtvectors_1 - skipthoughtvectors_1), skipthoughtvectors_1 * skipthoughtvectors_1]
     feature_df=pairtofeature(pairvector)
     mergeddf=df.join(feature_df)
     mergeddf.to_csv(outcsv_features)
     
    
if __name__ == '__main__':
   
    eval_sick.evaluate(model, evaltest=True)

   
   #eval_trec.evaluate(model, evalcv=False, evaltest=True)
    
    inputcsv1=os.path.join(projectdir,"data","sts-data-processed","test_2016.csv")
    inputcsv2=os.path.join(projectdir,"data","sts-data-processed","train_2012_to_2015.csv")
    datacolumn1="text_1"
    datacolumn2="text_2"
    outcsv_features1=os.path.join(projectdir,"data","sts-data-processed","test_2016_skipthought.csv")
    outcsv_features2=os.path.join(projectdir,"data","sts-data-processed","train_2012_to_2015_skipthought.csv")
    testskipthought(inputcsv1,outcsv_features1,datacolumn1,datacolumn2)
    testskipthought(inputcsv2,outcsv_features2,datacolumn1,datacolumn2)