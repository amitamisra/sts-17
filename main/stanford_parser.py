'''
Created on Oct 7, 2016
uses py4j to connect to a java program, java program should be running on the local host
at port 25335.

@author: amita
'''
from py4j.java_gateway import JavaGateway, GatewayParameters
gateway=JavaGateway(gateway_parameters=GatewayParameters(port=25335))

def parse_text(text):
    res=gateway.entry_point.parsed_text(text)
    return res


#change it to format as used in AFS. AFS was using stanford-corenlp-python.py wrapper but it works only with python2
#hence to use AFS the output parse from stanfordcorenlp is modified here 
def changeparsedtextformat(res):
    sentences=dict()
    all_sentences=[] 
    for sentence in res:
        newdict={}
        newdict["pos"]=sentence["pos"]
        newdict["lemmas"]=sentence["lemmas"]
        newdict["parse"]=str(sentence["parse"][0])
        newdict["deps_cc"]=sentence["deps_cc"]
        newdict["tokens"]=sentence["tokens"]
        newdict["char_offset"]=sentence["char_offset"]
        all_sentences.append(newdict)
    sentences["sentences"]=all_sentences    
    return sentences


if __name__ == '__main__':
    text="Law-abiding citizens are forbidden to carry guns. America is 1 in gun ownership it tanks only 107th in homicide rates."
    res=parse_text(text)
    print(res)
    
#Expected output
#[{'deps_cc': [['amod', 1, 0], ['nsubjpass', 3, 1], ['auxpass', 3, 2], ['mark', 5, 4], ['xcomp', 3, 5], ['dobj', 5, 6], ['punct', 3, 7], ['root', -1, 3]], 'pos': ['JJ', 'NNS', 'VBP', 'VBN', 'TO', 'VB', 'NNS', '.'], 'lemmas': ['law-abiding', 'citizen', 'be', 'forbid', 'to', 'carry', 'gun', '.'], 'tokens': ['Law-abiding', 'citizens', 'are', 'forbidden', 'to', 'carry', 'guns', '.'], 'parse': ['(ROOT (S (NP (JJ Law-abiding) (NNS citizens)) (VP (VBP are) (VP (VBN forbidden) (S (VP (TO to) (VP (VB carry) (NP (NNS guns))))))) (. .)))'], 'char_offset': [[0, 11], [12, 20], [21, 24], [25, 34], [35, 37], [38, 43], [44, 48], [48, 49]]}, {'deps_cc': [['nsubj', 2, 0], ['cop', 2, 1], ['case', 5, 3], ['compound', 5, 4], ['nmod:in', 2, 5], ['dep', 7, 6], ['nsubj', 9, 7], ['advmod', 9, 8], ['acl:relcl', 2, 9], ['case', 12, 10], ['compound', 12, 11], ['nmod:in', 9, 12], ['punct', 2, 13], ['root', -1, 2]], 'pos': ['NNP', 'VBZ', 'CD', 'IN', 'NN', 'NN', 'PRP', 'NNS', 'RB', 'VBP', 'IN', 'NN', 'NNS', '.'], 'lemmas': ['America', 'be', '1', 'in', 'gun', 'ownership', 'it', 'tank', 'only', '107th', 'in', 'homicide', 'rate', '.'], 'tokens': ['America', 'is', '1', 'in', 'gun', 'ownership', 'it', 'tanks', 'only', '107th', 'in', 'homicide', 'rates', '.'], 'parse': ['(ROOT (S (NP (NNP America)) (VP (VBZ is) (NP (NP (CD 1)) (PP (IN in) (NP (NN gun) (NN ownership))) (SBAR (S (NP (PRP it) (NNS tanks)) (ADVP (RB only)) (VP (VBP 107th) (PP (IN in) (NP (NN homicide) (NNS rates)))))))) (. .)))'], 'char_offset': [[50, 57], [58, 60], [61, 62], [63, 65], [66, 69], [70, 79], [80, 82], [83, 88], [89, 93], [94, 99], [100, 102], [103, 111], [112, 117], [117, 118]]}]
