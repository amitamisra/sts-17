#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on Jul 6, 2015
write some common file handling routines
@author: amita
'''

import csv, codecs, os
import json
from copy import deepcopy
import random
import pickle
from collections import OrderedDict
import pandas as pd

#concatenates a list of csvs, all  have same header
def concatenatecsvs(csvlist, separator,encod):
    frames=[]
    for csv in csvlist:
        df=pd.read_csv(csv, sep=separator, encoding=encod)
        frames.append(df)
    result=pd.concat(frames)
    return result
    
    

def traintestsplit(inputfeaturescsv,inputdatacsv,totalrecords, trainrecordscount,outputtrainfeatures,outputtraindata,outputtestfeatures,outputtestdata, labelcol):
    random.seed(0)
    rowdictsfeatures=readCsv(inputfeaturescsv)
    rowdictsdata=readCsv(inputdatacsv)
    combined=list(zip(rowdictsfeatures,rowdictsdata))
    random.shuffle(combined)
    
    rowdictsfeatures[:], rowdictsdata[:]=zip(*combined)

    train_features =rowdictsfeatures[:trainrecordscount]
    test_features = rowdictsfeatures[trainrecordscount:]
    
    train_data=    rowdictsdata[:trainrecordscount]
    test_data =    rowdictsdata[trainrecordscount:]
    
    colnamesfeatures=list(train_features[0].keys())
    colnamesdata=train_data[0].keys()
    
    colnamesfeatures.remove(labelcol)
    colnamesfeatures.append(labelcol) # for weka move target to last column
    writeCsv(outputtrainfeatures, train_features,colnamesfeatures)
    writeCsv(outputtraindata, train_data,colnamesdata)
    writeCsv(outputtestfeatures, test_features,colnamesfeatures)
    writeCsv(outputtestdata, test_data,colnamesdata)


# AllRows:list of dictionaries, read from csv
def addUniqueRowCol(AllRows):
    count=1
    AllNewRows=[]
    for row in AllRows:
        Newrow=deepcopy(row)
        Newrow["uniqueRowNo"]=count # gives each row a unique number for Mechanical turk
        count=count+1
        AllNewRows.append(Newrow)
    return AllNewRows 



#takes an input json   gives the rowdicts       
def jsonToRowDicts(listofdictfile): 
    with open(listofdictfile, 'r',encoding='utf-8') as fp:
        rowdicts = json.load(fp) 
    return  rowdicts

# write a list of dictionaries to a json file
def  writeJson(list_dict,filename):
    
    with open(filename, 'w') as f:
        json.dump(list_dict, f)

def readCsv(inputcsv):   
        inputfile = codecs.open(inputcsv,'r',encoding='utf-8') 
        result = list(csv.DictReader(inputfile))
        return result 

# write a csv, rowdicts:list of dictionaries,
#Assumption:  header is present in colnames
def writeCsv(outputcsv, rowdicts,colnames):
            restval=""
            extrasaction="ignore"
            dialect="excel"
            outputfile = codecs.open(outputcsv ,'w',encoding='utf-8' )
            csv_writer = csv.DictWriter(outputfile, colnames, restval, extrasaction, dialect, quoting=csv.QUOTE_NONNUMERIC)
            csv_writer.writeheader()
            csv_writer.writerows(rowdicts) 
            outputfile.close()


def readTextFile(Filename): 
        f = open(Filename, "r", encoding='utf-8')
        TextLines=f.readlines()
        f.close()
        return TextLines

def writeTextFile(Filename,Lines): 
        f = open(Filename, "w",encoding='utf-8')
        f.writelines(Lines)
        f.close()

def write_csv_header_unkown(filename, rowdicts, fieldnames=None, include_sorted_remaining_fields=True, get_keys_from_first_row=False, include_header=True, restval="", extrasaction="ignore", dialect="excel", *args, **kwds):
    """@note: Differs in the default extrasaction value
        @note: include_sorted_remaining_fields=True is expensive, so use get_keys_from_first_row=True when possible
        #TODO: Allow wildcards "*"
    
    """
    
    if fieldnames == None:
        fieldnames = []
    if include_sorted_remaining_fields:
        original_fields = set(fieldnames)
        additional_fields = set()
        for row in rowdicts:
            for key in row.keys():
                if key not in original_fields:
                    additional_fields.add(key)
            if get_keys_from_first_row:
                break
        fieldnames.extend(sorted(additional_fields))
    
    directory = os.path.dirname(filename)
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    outputfile = codecs.open(filename,'w','utf-8')
    csv_writer = csv.DictWriter(outputfile, fieldnames, restval, extrasaction, dialect, quoting=csv.QUOTE_NONNUMERIC, *args, **kwds)
    
    if include_header:
        #python 2.7 has this, 2.6 does not, this provides backwards compatibility
        if hasattr(csv_writer, 'writeheader'):
            csv_writer.writeheader()
        else:
            header = dict(zip(fieldnames, fieldnames))
            rowdicts.insert(0, header)

    return csv_writer.writerows(rowdicts)


def convertJsonToCsv( InputFileJson,OutputCSV):
    
            rowdicts=jsonToRowDicts(InputFileJson)
            fieldnames= sorted(rowdicts[0].keys())
            writeCsv(OutputCSV, rowdicts, fieldnames)
            
def convertCsvToJson( InputFileCsv,OutputJson):
    
            rowdicts=readCsv(InputFileCsv)
            writeJson(rowdicts,OutputJson)           


def readsPickle(pickleFile):
    with open(pickleFile, "rb") as input_file:
        data = pickle.load(input_file)

    return data  

def concatecsvWrite(input1,input2,input3,output):
    csvlist=[input1,input2,input3]
    res_dataframe=concatenatecsvs(csvlist,",","utf-8")
    res_dataframe.to_csv(output,index=False)
    
     
if __name__ == '__main__':
    
    InputFileCsv="/Users/amita/git/STS-17/data/sts-data-processed/train_2012_to_2015.csv"
    OutputJson="/Users/amita/git/STS-17/data/sts-data-processed/train_2012_to_2015.json"
    
    #convertCsvToJson( InputFileCsv,OutputJson)
    InputFileJson="/Users/amita/git/STS-17/data/sts-data-processed/train_2012_to_2015_corenlp.json"
    OutputCSV="/Users/amita/git/STS-17/data/sts-data-processed/train_2012_to_2015_corenlp.csv"
    convertJsonToCsv( InputFileJson,OutputCSV)
    
    
    
    
    