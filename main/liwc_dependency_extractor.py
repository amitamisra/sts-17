from feature_utils import annotation_to_dep_pairs, DependencyAlignmentException, log_length_normalization
from liwc_extractor import LiwcExtractor


class LiwcDependencyExtractor:
    def __init__(self, to_generalize):
        """
        :param to_generalize: which part to generalize: "gov" for govenor token, "dep"  for dependent token
        :return:
        """
        self.liwc_extractor = LiwcExtractor()
        if to_generalize != "gov" and to_generalize != "dep":
            raise ValueError("to_generalize only accepts \"gov\" or \"dep\"")
        self.to_generalize = to_generalize

    def extract(self, annotation_1, annotation_2):
        try:
            deps_1 = set(liwc_dep for dep in annotation_to_dep_pairs(annotation_1, "lemmas")
                      for liwc_dep in self._to_liwc_dep(dep))
            deps_2 = set(liwc_dep for dep in annotation_to_dep_pairs(annotation_2, "lemmas")
                      for liwc_dep in self._to_liwc_dep(dep))
        except DependencyAlignmentException:
            print("ERROR: Could not align dependency indices to tokens")
            return dict()

        norm_overlap = log_length_normalization(len(deps_1 & deps_2), annotation_1, annotation_2)
        simple_norm_overlap = log_length_normalization(
            len(self._to_simple_deps(deps_1) & self._to_simple_deps(deps_2)),
            annotation_1, annotation_2)

        return {"liwc_dep_overlap_norm": norm_overlap,
                "liwc_simplified_dep_overlap_norm": simple_norm_overlap}

    def _to_liwc_dep(self, dep):
        """
        Convert a dependency tuple to a liwc generalized dependency tuple
        :param dep:
        :return: A liwc generalized dependency
        """
        results = list()

        if self.to_generalize == "gov":
            for cat in self.liwc_extractor.score_token(dep[0]):
                results.append((cat, dep[1], dep[2]))
        elif self.to_generalize == "dep":
            for cat in self.liwc_extractor.score_token(dep[2]):
                results.append((dep[0], dep[1], cat))

        return results

    def _to_simple_deps(self, deps):
        return set((dep[0], dep[2]) for dep in deps)
