'''
Created on Oct 12, 2016

@author: amita
'''
import sys,os
import pandas as pd
current_dir=os.path.dirname(os.path.realpath(__file__) )
wrapper=(os.path.join(current_dir,"stanford-corenlp-python-master"))
sys.path.append( wrapper)
from corenlp import StanfordCoreNLP
corenlp_dir = "/Users/amita/software/stanford-corenlp-full-2015-04-20/"
corenlp = StanfordCoreNLP(corenlp_dir)  # wait a few minutes...
from corenlp import batch_parse
raw_text_directory="/Users/amita/git/STS-17/raw_text"
parsed = batch_parse(raw_text_directory, corenlp_dir, raw_output=True)
print (dict(parsed[0]))
#print(corenlp.raw_parse("The quick brown fox jumps over the lazy dog"))

def parse_file(x):
    parsed_doc1=corenlp.raw_parse(x["sentence_1"])
    parsed_doc2=corenlp.raw_parse(x["sentence_2"])
    
    #print(parsed_doc1)
    #print(parsed_doc2)    
    
if __name__ == '__main__':
    #parser=stanfordparser_utility.getparse()
    input_file="/Users/amita/git/STS-17/data/gc_test_corenlp.csv"
    #df=pd.read_csv(input_file)
    #sentence1="Gun free zones are zones where law-abiding citizens are forbidden to have guns because law-abiding citizens obey the law about gun free zones."
    #sentence2="Gun free zones are areas where law-abiding citizens are forbidden to carry guns."
    #parsed_doc1=corenlp.raw_parse("America is #1 in gun ownership it tanks only 107th in homicide rates.")
    #print(parsed_doc1)
    #df.apply(parse_file,axis=1)
