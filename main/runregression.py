'''
Created on Apr 2, 2016
run various regression algorithms, 
parameter tuning and feature selection can be done by pipeline and parameter grid 
There are a few default parameter setting for each regressor. 
These can be changed using a csv input
The function run reads a csv containing a sequence of train,test and regression models to run . parameter can be set in this csv
The final output contains actual and predicted scores written to a csv, alongwith regression metrics.
Fields in Csv
inputTrain
inputTest
outputBaseDir
featureList
eval_type_list: can be CV or test
class_label
done: if 1, then skips this row
reg_type: classifier could be SVM/LR
gammastart
gammaend
Cstart
Cend
alphastart
alphaend
( These paremeters for svm and ridge regression)
scoring: scoring critertia for nested CV
Featureselection: by default I am using auto for all the features, You can give a list of features like[10,20,40]
featurelist: is the specific feature names you want before any feature selection is done. They should be separated by comma.
you can just specify initial distinguishing characters for features. 
For example to keep unigram_i,bigram_i_am  as features, the feature list should be unigram, bigram
It then creates  a directory baseoutput/featurelist/classifier/featurelist_CV.csv for nested cross validation
It then creates  a directory baseoutput/featurelist/classifier/featurelist_test.csv for evaluation on nested test set
CV follds=10, can be changed in the script, See default settings below 
@author: amita
'''
import configparser

import pandas as pd, os
import regression
import  file_utilities
import logging
import gensim
from scipy import spatial
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

PROJECT_DIR = os.path.dirname(os.path.abspath((os.path.join(os.path.dirname(__file__), ".."))))
RESOURCE_DIR = os.path.abspath(os.path.join(PROJECT_DIR, "resources"))
defaultalphastart=1
defaultalphaend=10
defaultalphacount=10
defaultgammastart=1
defaultgammaend=30
defaultCstart=1
defaultCend=60
defaultgammacount=10
defaultCcount=10
defaultscoring="mean_squared_error"
defaultfolds=10

def splifeatures_label(inputcsv,class_label,feature_list):
    df=pd.read_csv(inputcsv,na_filter=False)
    colnames=list(df.columns.values)
    feature_cols=[col for col in colnames if str(col).startswith(tuple(feature_list))]
    Y_values= df[class_label].values
    X_values= df[list(feature_cols)].values
    return(X_values,Y_values)





def executesvm(X_train,Y_train,eval_type_list,scoringcriteria,gammastart,gammaend,gammacount,Cstart,Cend,Ccount,folds,num_features,param_dict_regression):
    logger.info("exceuting svm")
    logger.info(param_dict_regression["featureList"])
    svmregressor= regression.SVMRegression(num_features,gammastart,gammaend,gammacount,Cstart,Cend,Ccount)
    svmregressor.setpipeline()
    svmregressor.setparam_grid()
    result_dict_Test={}
    result_dict_CV={}
    if "CV" in eval_type_list:
        result_dict_CV=regression.gridsearchnestedCV(X_train,Y_train,svmregressor.pipeline,svmregressor.param_grid,scoringcriteria, folds,param_dict_regression)
        
    if "test" in eval_type_list:
        inputtest=param_dict_regression["inputTest"]
        feature_list=param_dict_regression["featureList"]
        class_label=param_dict_regression["class_label"]
        X_test,Y_test=splifeatures_label(inputtest,class_label,feature_list)
        result_dict_Test=regression.gridSearchTestSet(X_train, Y_train,X_test,Y_test, svmregressor.pipeline,svmregressor.param_grid, scoringcriteria, folds,param_dict_regression)
    
    if "scorefolds" in eval_type_list:
        regression.gridsearchnestedCVfoldsTTest(X_train,Y_train,svmregressor.pipeline,svmregressor.param_grid,scoringcriteria,folds,param_dict_regression)
        
    logger.info("Done executing svm :  "+ str(param_dict_regression["featureList"]))
    return(result_dict_CV,result_dict_Test)





def executeRidgeregression(X_train,Y_train,eval_type_list,scoringcriteria,alphastart,alphaend, alphacount,num_features,folds,param_dict_regression):
    logger.info("exceuting ridgeregression")
    logger.info(param_dict_regression["featureList"])
    ridgeregressor=regression.RidgeRegression(num_features,alphastart,alphaend, alphacount)
    ridgeregressor.setpipeline()
    ridgeregressor.setparam_grid()
    result_dict_Test={}
    result_dict_CV={}
    if "CV" in eval_type_list:
        result_dict_CV=regression.gridsearchnestedCV(X_train,Y_train,ridgeregressor.pipeline,ridgeregressor.param_grid,scoringcriteria,folds,param_dict_regression)
    if "test" in eval_type_list:
        inputtest=param_dict_regression["inputTest"]
        feature_list=param_dict_regression["featureList"]
        class_label=param_dict_regression["class_label"]
        X_test,Y_test=splifeatures_label(inputtest,class_label,feature_list)
        result_dict_Test=regression.gridSearchTestSet(X_train, Y_train,X_test, Y_test, ridgeregressor.pipeline,ridgeregressor.param_grid,scoringcriteria,folds,param_dict_regression)
    logger.info("Done executing Ridge:  "+ str(param_dict_regression["featureList"]))
    
    if "scorefolds" in eval_type_list:
        regression.gridsearchnestedCVfoldsTTest(X_train,Y_train,ridgeregressor.pipeline,ridgeregressor.param_grid,scoringcriteria,folds,param_dict_regression)
        
    return(result_dict_CV,result_dict_Test)


def executeGBRregression(X_train,Y_train,eval_type_list,scoringcriteria,alphastart,alphaend, alphacount,num_features,defaultfolds,param_dict_regression):
    logger.info("exceuting GBRregression")
    logger.info(param_dict_regression["featureList"])
    gradientRegressor=regression.GradientBoostingRegression(num_features,alphastart,alphaend, alphacount)
    gradientRegressor.setpipeline()
    gradientRegressor.setparam_grid()
    result_dict_Test={}
    result_dict_CV={}
    if "CV" in eval_type_list:
        result_dict_CV=regression.gridsearchnestedCV(X_train,Y_train,gradientRegressor.pipeline,gradientRegressor.param_grid,scoringcriteria,defaultfolds,param_dict_regression)
    if "test" in eval_type_list:
        inputtest=param_dict_regression["inputTest"]
        feature_list=param_dict_regression["featureList"]
        class_label=param_dict_regression["class_label"]
        X_test,Y_test=splifeatures_label(inputtest,class_label,feature_list)
        result_dict_Test=regression.gridSearchTestSet(X_train, Y_train,X_test, Y_test, gradientRegressor.pipeline,gradientRegressor.param_grid,scoringcriteria,defaultfolds,param_dict_regression)
    return(result_dict_CV,result_dict_Test)

def execute(param_dict_regression):
    inputtrain= param_dict_regression["inputTrain"]
    featureList=param_dict_regression["featureList"]
    reg_type=param_dict_regression["reg_type"]
    eval_type_list=param_dict_regression["eval_type_list"]
    class_label=param_dict_regression["class_label"]
    num_features=param_dict_regression["Num_features_selection"]
    if param_dict_regression["scoring"]:
        scoringcriteria=param_dict_regression["scoring"]
    else:
        scoringcriteria=defaultscoring  
        
    train_df=pd.read_csv(inputtrain,na_filter=False)
    colnames=list(train_df.columns.values)
    
         
    feature_cols=[col for col in colnames if str(col).startswith(tuple(featureList))]
    Y_train= train_df[class_label].values
    X_train = train_df[list(feature_cols)].values
    param_dict_regression["feature_cols_included"]=feature_cols
    
    if "SVM" == reg_type:
        
        if param_dict_regression["gammastart"]:
            gammastart=param_dict_regression["gammastart"]
        else:
            gammastart=defaultgammastart
            
        if param_dict_regression["gammaend"]:
            gammaend=param_dict_regression["gammaend"] 
        else:
            gammaend=defaultgammaend 
        if param_dict_regression["gammacount"]:
            gammacount= param_dict_regression["gammacount"]
        else:
            gammacount=defaultgammacount       
               
        if param_dict_regression["Cstart"]:
            Cstart=param_dict_regression["Cstart"]
        else:
            Cstart=defaultCstart
            
        if param_dict_regression["Cend"]:
            Cend=param_dict_regression["Cend"] 
        else:
            Cend=defaultCend 
            
        if param_dict_regression["Ccount"]:
            Ccount=param_dict_regression["Ccount"] 
        else:
            Ccount=defaultCcount               
        
        result_dict_CV,result_dict_Test=executesvm(X_train,Y_train,eval_type_list,scoringcriteria,gammastart,gammaend,gammacount,Cstart,Cend,Ccount,defaultfolds,num_features,param_dict_regression)
    

     
    if "Ridge"==reg_type:
        if param_dict_regression["alphastart"]:
            alphastart=param_dict_regression["alphastart"]
        else:
            alphastart=defaultalphastart
            
        if param_dict_regression["alphaend"]:
            alphaend=param_dict_regression["alphaend"] 
        else:
            alphaend=defaultalphaend
            
        if param_dict_regression["alphacount"]:
            alphacount=param_dict_regression["alphacount"] 
        else:
            alphacount=defaultalphacount
            
        result_dict_CV,result_dict_Test=executeRidgeregression(X_train,Y_train,eval_type_list,scoringcriteria,alphastart,alphaend, alphacount,num_features,defaultfolds,param_dict_regression)
        
    
    
    if "GBR"==reg_type:
        if param_dict_regression["alphastart"]:
            alphastart=param_dict_regression["alphastart"]
        else:
            alphastart=defaultalphastart
            
        if param_dict_regression["alphaend"]:
            alphaend=param_dict_regression["alphaend"] 
        else:
            alphaend=defaultalphaend
            
        if param_dict_regression["alphacount"]:
            alphacount=param_dict_regression["alphacount"] 
        else:
            alphacount=defaultalphacount
        result_dict_CV,result_dict_Test=executeGBRregression(X_train,Y_train,eval_type_list,scoringcriteria,alphastart,alphaend, alphacount,num_features,defaultfolds,param_dict_regression)

    
    return(result_dict_CV,result_dict_Test) 

def createresultdict(param_dict_regression,result_dict,eval_type):   
    newdict={}
    newdict["inputTrain"]=param_dict_regression["inputTrain"] 
    newdict["inputTest"]=param_dict_regression["inputTest"]    
    newdict["reg_type"]=param_dict_regression["reg_type"]
    newdict["feature_cols_included"]=param_dict_regression["feature_cols_included"]
    newdict["feature_list"]=str(param_dict_regression["featureList"])
    newdict["rmse"]=result_dict["rmse"]
    newdict["rrse"]=result_dict["rrse"]
    newdict["rsquare"]=result_dict["rsquare"]
    newdict["r"]=result_dict["r"]
    newdict["Eval_type"]=eval_type
    newdict["param_grid"]=result_dict["param_grid"]
    newdict["best_param"]=result_dict["best_param"]
    return newdict
    
def addresult(result_dict_CV,result_dict_Test,param_dict_regression,all_list):
    
  
    if result_dict_CV:
        newdict=createresultdict(param_dict_regression,result_dict_CV,"CV")
        all_list.append(newdict)
    if result_dict_Test:
        newdicttest=createresultdict(param_dict_regression,result_dict_Test,"test")
        all_list.append(newdicttest)
        


    
    
   
def run(section):
    config = configparser.ConfigParser()
    regressioninput=file_utilities.get_absolutepath_data("config","regression.ini")
    config.read(regressioninput)
    input_file_config=config.get(section,"csv_with_file_list")
    resultCSVFile_config=config.get(section,'resultCSVFile')
    input_file=file_utilities.get_absolutepath_data(input_file_config)
    resultCSVFile=file_utilities.get_absolutepath_data(resultCSVFile_config) 
    inputdata_df=pd.read_csv(input_file)
    inputdata_df.fillna(0)
    
    all_list=[]
    if os.path.exists(resultCSVFile):
            os.remove(resultCSVFile)
    for index, row in inputdata_df.iterrows():
        param_dict_regression={}
        done=row["done"]
        if int(done)==1:
            continue
        #addcosine(row["inputTrain"])
        param_dict_regression["gammastart"]=row["gammastart"]
        param_dict_regression["gammaend"]=row["gammaend"]
        param_dict_regression["gammacount"]=row["gammacount"]
       
        param_dict_regression["Cstart"]=row["Cstart"]
        param_dict_regression["Cend"]=row["Cend"]
        param_dict_regression["Ccount"]=row["Ccount"]
       
        param_dict_regression["alphastart"]=row["alphastart"]
        param_dict_regression["alphaend"]=row["alphaend"]
        param_dict_regression["alphacount"]=row["alphacount"]
        
        param_dict_regression["scoring"]=row["scoring"]
     
        param_dict_regression["reg_type"]=row["reg_type"]
        param_dict_regression["featureList"]=list(row["featureList"].split(","))
        param_dict_regression["eval_type_list"]=row["eval_type_list"]
        param_dict_regression["class_label"]=row["class_label"]
        param_dict_regression["Num_features_selection"]=list(str(row["Featureselection"]).split(","))# A list of possible  feature count  for feature selection, by default no feature selection
        
        param_dict_regression["inputTrain"]=file_utilities.get_absolutepath_data(row["inputTrain"])
        param_dict_regression["inputTest"]=file_utilities.get_absolutepath_data(row["inputTest"])
        outBaseDir=file_utilities.get_absolutepath_data(row["outputBaseDir"])
        if not os.path.exists(outBaseDir):
            os.mkdir(outBaseDir)
        param_dict_regression["outputBaseDir"]= outBaseDir   
        result_dict_CV,result_dict_Test=execute(param_dict_regression)  
        addresult(result_dict_CV,result_dict_Test,param_dict_regression,all_list)
    
    
    if all_list:
        df=pd.DataFrame(all_list)
        df.to_csv(resultCSVFile,index=False)           
    
if __name__ == '__main__':
   
    section="afs"
    run(section)
   