'''
Created on Mar 12, 2016
creates different regression types using pipeline and parameter grid.
Each regressor has  a default pipeline and grid. It can be changed when creating an instance of the class
@author: amita
'''
from math import sqrt
import os, codecs
from pprint import pprint
from numpy.ma import corrcoef
from sklearn import cross_validation 
from sklearn import gaussian_process
from sklearn import preprocessing
from sklearn import svm
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_regression
from sklearn.grid_search import GridSearchCV
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.pipeline import Pipeline
from sklearn.ensemble import GradientBoostingRegressor
import pandas as pd
import numpy as np
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

PROJECT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
RESOURCE_DIR = os.path.abspath(os.path.join(PROJECT_DIR, "resources"))



class GradientBoostingRegression:
    def __init__(self,numFeatureSelection,alphastart,alphaend,alphacount):
        self.numFeatureSelection= [int(x) for x in numFeatureSelection if x.isdigit()] + ["all"]
        self.alphastart=float(alphastart)
        self.alphaend=float(alphaend)
        self.alphacount=alphacount
      
    def setpipeline(self):
        self.pipeline=Pipeline([('scale', preprocessing.StandardScaler()),('filter', SelectKBest(f_regression)),("gbr", GradientBoostingRegressor(random_state=42, max_features="sqrt"))])
        
    def setparam_grid(self):
        alpha_range = np.logspace(self.alphastart, self.alphaend, self.alphacount)
        self.param_grid=[{'filter__k': self.numFeatureSelection, "gbr__n_estimators": list(range(100, 501, 100)),
        "gbr__loss":["lad"],
        "gbr__max_depth": list(range(3, 11)),
        "gbr__learning_rate":  alpha_range}] 
    
  

class RidgeRegression:
    def __init__(self,numFeatureSelection,alphastart,alphaend, alphacount):
        self.numFeatureSelection= [int(x) for x in numFeatureSelection if x.isdigit()] + ["all"]
        self.alphastart=float(alphastart)
        self.alphaend=float(alphaend)
        self.alphacount=alphacount
      
    def setpipeline(self):
        self.pipeline=Pipeline([('scale', preprocessing.StandardScaler()),('filter', SelectKBest(f_regression)),('rr',Ridge())])
        
    def setparam_grid(self):
        alpha_range = np.logspace(self.alphastart, self.alphaend, self.alphacount)
        self.param_grid=[{'filter__k': self.numFeatureSelection, "rr__alpha":alpha_range}] 
    
    



class SVMRegression:
    
    def __init__(self,numFeatureSelection,gammastart,gammaend,gammacount,Cstart,Cend,Ccount):
        self.gammastart=float(gammastart)
        self.gammaend=float(gammaend)
        self.gammacount=float(gammacount)
        self.Cstart=float(Cstart)
        self.Cend=float(Cend)
        self.Ccount=float(Ccount)
        self.numFeatureSelection= [int(x) for x in numFeatureSelection if x.isdigit()] + ["all"]
    def setpipeline(self):
        self.pipeline=Pipeline([('scale', preprocessing.StandardScaler()),('filter', SelectKBest(f_regression)),('svr', svm.SVR())])
        
    def setparam_grid(self):
        C_range = np.logspace(self.Cstart, self.Cend, self.Ccount)
        gamma_range = np.logspace(self.gammastart,self.gammaend,self.gammacount)
        #self.param_grid=[{'svr__kernel': ['linear'], 'svr__C': C_range,'filter__k': self.numFeatureSelection}]
        self.param_grid=[{'svr__kernel': ['rbf'], 'svr__gamma': gamma_range,'svr__C': C_range, 'filter__k': self.numFeatureSelection}]
        
        
          


def writeresultstoFile(csvFile,result_dict,pred_list,X_train,Y_train,param_dict_regression):
    logger.info("start writing file " + str(param_dict_regression["featureList"]) +" "+csvFile  )
    assert(len(pred_list)==len(X_train)==len(Y_train))
    
    df1=pd.DataFrame(pred_list)
    df2=pd.DataFrame(X_train)
    df = df2.join(df1)
    df.to_csv(csvFile,index=False)
    f = codecs.open(csvFile, "a",encoding='utf-8')
    pprint(result_dict, stream=f) 
    pprint(param_dict_regression["feature_cols_included"], stream=f)
    f.close()
    logger.info("Done writing file " + str(param_dict_regression["featureList"]) +" "+csvFile  )

#performs nested cross validation using grid search, pipeline contains the regressor and its parameters are evaluated using parameter grid    
#use it to report cross validation accuracy  
def gridsearchnestedCV(X_train,Y_train,pipeline, param_grid,scoringcriteria,cvfolds,param_dict_regression):
        logger.info("Doing grid search for cv")
        grid_search = GridSearchCV(pipeline, param_grid=param_grid,scoring=scoringcriteria,cv=5)
        Y_pred=cross_validation.cross_val_predict(grid_search, X_train, Y_train,cv=cvfolds,n_jobs=-1)
        result_dict={}
        pred_list= [{'Y_actual': v1, 'Y_predicted': v2} for v1, v2 in zip(Y_train, Y_pred)]
        result_dict["inputTrain"]=param_dict_regression["inputTrain"]
        result_dict["r"] =corrcoef(Y_train,Y_pred)[0, 1]
        rmse=result_dict["rmse"]=sqrt(mean_squared_error(Y_train,Y_pred))
        rrse = rmse / np.sqrt(mean_squared_error(Y_train, np.repeat(Y_train.mean(), len(Y_train))))
        result_dict["rrse"]=rrse
        result_dict["rsquare"]=r2_score(Y_train,Y_pred)
        result_dict["param_grid"]=param_grid
        
        
        #grid_search.fit(X_train, Y_train)
        #best_param=grid_search.best_params_# Use it only to see what was the best parameter, do not use for reporting results
        best_param=""
        result_dict["best_param"]=best_param
        basedir=param_dict_regression["outputBaseDir"]
        classifier=param_dict_regression["reg_type"]
        feature= "_".join(param_dict_regression["featureList"])
        outputDir=os.path.join(basedir,feature)
        if not os.path.exists(outputDir):
            os.makedirs(outputDir)
            
        csvFile=os.path.join(outputDir,feature+classifier+"__CV.csv")
        writeresultstoFile(csvFile,result_dict, pred_list,X_train,Y_train,param_dict_regression)
        return result_dict

#performs nested cross validation using grid search, pipeline contains the regressor and its parameters are evaluated using parameter grid    
#use it to report cross validation accuracy, get scores for each fold for pairdTTest
def gridsearchnestedCVfoldsTTest(X_train,Y_train,pipeline, param_grid,scoringcriteria,cvfolds,param_dict_regression):
        logger.info("Doing grid search for cvfolds") 
        grid_search = GridSearchCV(pipeline, param_grid=param_grid,scoring=scoringcriteria,cv=5,n_jobs=-1)
        scores = cross_validation.cross_val_score(grid_search, X_train, Y_train,cv=cvfolds)
        feature= "_".join(param_dict_regression["featureList"])
        colname= "scores:"+ scoringcriteria
        basedir=param_dict_regression["outputBaseDir"]
        classifier=param_dict_regression["reg_type"]
        score_list= [{colname: v1} for v1 in scores]
        outputDir=os.path.join(basedir,feature)
        if not os.path.exists(outputDir):
            os.makedirs(outputDir)
            
        csvFile=os.path.join(outputDir,feature+classifier+"__PairedTTestCV.csv")
        df1=pd.DataFrame(score_list)
        df1.to_csv(csvFile,index=False)
        f = codecs.open(csvFile, "a",encoding='utf-8')
        pprint(param_dict_regression["feature_cols_included"], stream=f)
        pprint(("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2)),stream=f)
        pprint("rmse: " +str(sqrt(scores.mean() * -1)),stream=f)
        pprint("scoringcriteria "+ scoringcriteria,stream=f)
        f.close()
      
        
        
#performs nested cross validation using grid search, gets the best parameter and gives performance on separate held out test set
#use it to report accuracy on held out test set
def gridSearchTestSet(X_train,Y_train,X_test,Y_test,pipeline,param_grid,scoringcriteria,cvfolds,param_dict_regression): 
        logger.info("Doing grid search for test set")
        result_dict={}
        grid_search = GridSearchCV(pipeline, param_grid=param_grid,scoring=scoringcriteria,cv=3,n_jobs=-1)       
        grid_search.fit(X_train, Y_train)
        best_param=grid_search.best_params_
        
        Y_pred=grid_search.predict(X_test)
        pred_list= [{'Y_actual': v1, 'Y_predicted': v2} for v1, v2 in zip(Y_test, Y_pred)]
        result_dict["inputTrain"]=param_dict_regression["inputTrain"]
        result_dict["inputTest"]=param_dict_regression["inputTest"]
        result_dict["r"] =corrcoef (Y_test,Y_pred)[0, 1]
        rmse=result_dict["rmse"]=sqrt(mean_squared_error(Y_test,Y_pred))
        result_dict["rsquare"]=r2_score(Y_test,Y_pred)
        rrse = rmse / np.sqrt(mean_squared_error(Y_test, np.repeat(Y_test.mean(), len(Y_test))))
        result_dict["rrse"]=rrse
        result_dict["best_param"]=best_param
        result_dict["param_grid"]=param_grid
        
        basedir=param_dict_regression["outputBaseDir"]
        classifier=param_dict_regression["reg_type"]
        feature= "_".join(param_dict_regression["featureList"])
        outputDir=os.path.join(basedir,feature)
        if not os.path.exists(outputDir):
            os.makedirs(outputDir)
            
        csvFile=os.path.join(outputDir,feature + classifier +"__test.csv")
        writeresultstoFile(csvFile,result_dict,pred_list,X_test,Y_test,param_dict_regression)
        return result_dict


