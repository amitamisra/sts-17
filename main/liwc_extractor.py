#!/usr/bin/env python2
# coding: utf8

import pandas

import file_utilities
import word_category_counter
from feature_utils import log_length_normalization, normalized_dict_overlap, flatten_annotation_to_tokens


class LiwcExtractor:
    def __init__(self):
        # Setup the dictionary
        word_category_counter.load_dictionary(file_utilities.load_resource("liwc/LIWC2007.dic"))
        # Read the categories we want to include from liwc_process_type.csv
        df = pandas.read_csv(file_utilities.load_resource("liwc/liwc_process_type.csv"))
        self.included_categories = set(cat.strip().lower() for cat in df.loc[df["ignore_sim"] != 1]["category"])

    def extract(self, annotation_1, annotation_2):
        """
        Run the liwc scoring script for both annotations and do a normalized overlap
        :param annotation_1: Annotation like dictionary
        :param annotation_2: Annotation like dictionary
        :return:
        """
        t1 = self.score_tokens(flatten_annotation_to_tokens(annotation_1, "tokens"))
        t2 = self.score_tokens(flatten_annotation_to_tokens(annotation_2, "tokens"))

        def norm(x):
            return log_length_normalization(x, annotation_1, annotation_2)

        return normalized_dict_overlap(t1, t2, norm)

    def score_tokens(self, tokens):
        return {key.strip().lower(): val
                for key, val in word_category_counter.score_text(None, all_tokens=tokens, raw_counts=True).items()
                if key.strip().lower() in self.included_categories}

    def score_token(self, token):
        return self.score_tokens([token])
