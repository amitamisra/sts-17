#!/usr/bin/env python2
# coding: utf8

'''
Created on OCtober 8, 2016

@author: amita
'''

import time
from collections import ChainMap
from feature_utils import build_ngrams
from liwc_dependency_extractor import LiwcDependencyExtractor
from liwc_extractor import LiwcExtractor
from ngram_extractor import NgramExtractor
from rouge import rouge
from umbc_sim import UmbcSimilarity
import os
import pandas as pd
import configparser, logging
import stanford_parser
#from collections import ChainMap
from terp import score_terp
from word2vec_extractor import Word2vecExtractor
from timeit import default_timer as timer
import file_utilities
#import skip_thought_vectors_py3.addskipthought as addskipthought
logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)

# only tested for w2vec","liwc_dep","liwc","umbc","rouge","ngram
features=["w2vec","liwc_dep","liwc","umbc","rouge","ngram"]
#features=["liwc"]
#keep track of progess use a global variable
globalcounter=0

def addNgramfeats(parsed_doc1,parsed_doc2):
    extractor = NgramExtractor(3, "lemmas")
    Ngramfeats = extractor.extract(parsed_doc1, parsed_doc2)
    return Ngramfeats

def addRougefeats(sentence1, sentence2):
    rougescores = rouge(sentence1, sentence2)
    return rougescores


def addUMBCsimfeats(sentence1, sentence2):
    success=False
    while not success:
        try:
            sim = UmbcSimilarity.get_sim(sentence1, sentence2)
            success=True
        except:
            logger.info("An Exception from UMBC, sleep for 30 sec,try again")
            time.sleep(30)    
    return sim

def addLIWCfeats(parsed_doc1, parsed_doc2):  
    extractor = LiwcExtractor()
    liwcscores = extractor.extract(parsed_doc1, parsed_doc2)
    return liwcscores

def addLIWCdepfeatures(parsed_doc1, parsed_doc2):
    extractor = LiwcDependencyExtractor("gov")
    liwc_depfeats = extractor.extract(parsed_doc1, parsed_doc2)
    return liwc_depfeats
    
def addterpfeats(sentence1, sentence2,terp_loc):   
    terp_feats=score_terp(sentence1, sentence2,terp_loc) 
    return terp_feats

def addw2vecfeatures(sentence1,sentence2,W2vecextractor):
    #extractor=Word2vecExtractor(w2vec_loc)
    
    w2vec_scores=W2vecextractor.extract(sentence1,sentence2)
    return w2vec_scores
          
def applyfeatures(x,datacolumn1,datacolumn2,terp_loc,W2vecextractor,parsecolumn1,parsecolumn2,isfileparsed):
    global globalcounter
    globalcounter=globalcounter+1
    if  isfileparsed==0:
        parsed_doc1=stanford_parser.changeparsedtextformat(stanford_parser.parse_text(x[datacolumn1]))
        parsed_doc2=stanford_parser.changeparsedtextformat(stanford_parser.parse_text(x[datacolumn2]))
    else:
        parsed_doc1=x[parsecolumn1]
        parsed_doc2=x[parsecolumn2]    
    logger.info("Doing Feature generation for record:{0}".format(globalcounter))
    logger.info("text column 1:{0}".format(x[datacolumn1]))
    logger.info("text column 2:{0}".format(x[datacolumn2]))
    liwc_depfeatures={}
    liwc_scores_dict={}
    rougescores={}
    ngramscores={}
    umbc_sim={}
    terpscores={}
    w2vec_scores={}
    
    
    if "liwc" in features:
        liwc_scores=addLIWCfeats(parsed_doc1, parsed_doc2)
        liwc_scores_dict={}
        for k,v in liwc_scores.items(): 
            liwc_scores_dict["LIWC_"+k+"overlap_norm"]=liwc_scores[k]
    if "liwc_dep" in features:    
        liwc_depfeatures=addLIWCdepfeatures(parsed_doc1, parsed_doc2)
    if "umbc" in features:
       # print("adding LIWC_DEP_Overlap features")
        umbc_score=addUMBCsimfeats(x[datacolumn1],x[datacolumn2])
        umbc_sim={"umbc_simi":umbc_score}
    if "rouge" in features:    
        rougescores=addRougefeats(x[datacolumn1],x[datacolumn2])
    if "ngram" in features:   
        ngramscores=addNgramfeats(parsed_doc1,parsed_doc2)
    if "terp" in features:    
        terpscores=addterpfeats(x[datacolumn1], x[datacolumn2],terp_loc)
    if "w2vec" in features:
        w2vec_scores=addw2vecfeatures(x[datacolumn1],x[datacolumn2],W2vecextractor)
    #z= dict(ChainMap(liwc_depfeatures,  liwc_scores,rougescores,ngramscores,umbc_sim,terpscores,w2vec_scores))
    z= dict(ChainMap(liwc_depfeatures, liwc_scores_dict,rougescores,ngramscores,terpscores,umbc_sim,w2vec_scores))

    #print (z)
    return pd.Series(z)


def addskipfeatures(input_file,datacolumn1,datacolumn2):
    """
    df=pd.read_csv(input_file)
    skip_featuresdf=addskipthought(df,datacolumn1,datacolumn2)
    mergeddf=df.join(skip_featuresdf)
    mergeddf.to_csv(input_file)
    """

def buildfeatures(input_file,features_file,datacolumn1,datacolumn2,w2vec_loc,terp_loc,srparser_loc,parse_model,parsecolumn1,parsecolumn2,isfileparsed):
    if "w2vec" in features:
        W2vecextractor=Word2vecExtractor(w2vec_loc)   
    else:
        W2vecextractor=""                                       
    
    if  "skip-thought" in features:
        addskipfeatures(input_file,datacolumn1,datacolumn2)
        
    df=pd.read_csv(input_file)   
    start = timer()
    newdf=df.apply(applyfeatures,args=(datacolumn1,datacolumn2,terp_loc,W2vecextractor,parsecolumn1,parsecolumn2,isfileparsed),axis=1)
    newdf.fillna(0,inplace=True)
    merged_df= df.join(newdf)
    end = timer()
    print("feature generation for {0}took".format(str(features)))
    print(end - start)      
    merged_df.to_csv(features_file)
    
def run(section):
    args= readCongigFile(section)
    inputfile=args[0]
    features_file=args[1]
    datacolumn1=args[2]
    datacolumn2=args[3]
    w2vec_loc=args[4]
    terp_loc=args[5]
    srparser_loc=args[6]
    parse_model=args[7]
    parsecolumn1=args[8]
    parsecolumn2=args[9]
    isfileparsed=int(args[10])
    buildfeatures(inputfile,features_file,datacolumn1,datacolumn2,w2vec_loc,terp_loc,srparser_loc,parse_model,parsecolumn1,parsecolumn2,isfileparsed)   
      

def readCongigFile(section):
    config = configparser.ConfigParser()
    config_file=file_utilities.get_absolutepath_data("config","buildfeatures_Config.ini")
    config.read(config_file)
    
   
    datacolumn1=config.get(section,'datacolumn1')
    datacolumn2=config.get(section,'datacolumn2')
    
    w2vec_loc=config.get(section,'w2vec')
    terp_loc=config.get(section,'terp_loc')
    srparser_loc=config.get(section,'srparser_loc')
    parse_model=config.get(section,'parse_model')
    
    input_file_config=config.get(section,"input_file")
    feature_file_config=config.get(section,'feature_file')
   
    input_file=file_utilities.get_absolutepath_data(input_file_config)
    feature_file=file_utilities.get_absolutepath_data(feature_file_config) 
    parsecolumn1=config.get(section,"parsecolumn1")
    parsecolumn2=config.get(section,"parsecolumn2")
    isfileparsed=config.get(section,"isfileparsed")
    arguments=(input_file,feature_file,datacolumn1,datacolumn2,w2vec_loc, terp_loc,srparser_loc,parse_model,parsecolumn1,parsecolumn2,isfileparsed)

    
    return  (arguments)
    
if __name__ == '__main__':
    section="2012-2015_train"
    run(section)
