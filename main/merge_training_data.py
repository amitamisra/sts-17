#!/usr/local/bin/python2.7
# encoding: utf-8
'''
main.merge_training_data -- shortdesc


'''

import sys
import os,configparser
import pandas as pd

def run():
    args= readCongigFile()
    input_csv= args[0]
    output_csv=args[1]
    #execute(input_csv,output_csv,good_worker_file)

def createpairs_gold_standard(pairs_file,gold_std,outfile):
    df_pairs=pd.read_table(pairs_file, sep='\t', lineterminator='\n',header=None)
    df_gs=pd.read_csv(gold_std, sep='\t', lineterminator='\n',header=None)
    result = pd.concat([df_pairs, df_gs], axis=1, join='inner')
    result.columns=["sentence_1","sentence_2","regression_label"]
    result.to_csv(outfile,index=False)
    
    

def readCongigFile(outfile):
    config = configparser.ConfigParser()
    config.read('merge_training_data_Config.ini')
    input_csv = config.get('train', 'input_csv')
    output_csv= config.get('train', 'output_train_csv')
    arguments=(input_csv,output_csv)
    return  (arguments)

if __name__=="__main__":
    pairs_file="/Users/amita/STS-17/sts-en-test-gs-2014/STS.input.deft-forum.txt"
    gold_std="/Users/amita/STS-17/sts-en-test-gs-2014/STS.gs.deft-forum.txt"
    dir_path = os.path.dirname(os.path.realpath(__file__))
    data_dir=os.path.join(os.path.dirname(dir_path),"data")
    outfile=os.path.join(data_dir,"input.deft-forum.csv")
    createpairs_gold_standard(pairs_file,gold_std,outfile)
    