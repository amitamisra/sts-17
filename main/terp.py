#!/usr/bin/env python2
# coding: utf8

'''
Created on Sep 27, 2016

@author: amita <refset
'''
from bs4 import BeautifulSoup
import subprocess,os,sys,shutil
import tempfile, codecs
from unidecode import unidecode

def convert_textxml(text,type_text):
    '''
    change text to xml format as required by terp software
    '''
    assert type_text=="reference1" or type_text=="reference2"
    if type_text=="reference1":
        val1="tstset"
        val2="ref1"
    else:
        val1="refset" 
        val2="ref2"
    markup=""" <?xml version="1.0" encoding="UTF-8"?> 
               <{0} trglang="en" setid="1" srclang="any">
               <doc sysid={1} docid="1" >
               <body></body>
           """     .format(val1,val2)
           
    soup = BeautifulSoup(markup,"lxml") 
    body_tag=soup.body
    seg_tag=soup.new_tag('seg', id=1)   
    seg_tag.string=text
    body_tag.append(seg_tag)
    return soup


def score_terp(text1,text2,path_to_terp):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    tmpdir = tempfile.mkdtemp()
    soup1=convert_textxml(text1,"reference1")
    file1=os.path.join(tmpdir,"file1.html")
    with codecs.open(file1, 'w',encoding="utf-8") as outfile1:
        outfile1.write(soup1.prettify())
        
    file2=os.path.join(tmpdir,"file2.html")
    soup2=convert_textxml(text2,"reference2")
    with codecs.open(file2, 'w',encoding="utf-8") as outfile2:
        outfile2.write(soup2.prettify())    
    #cmd=str("{0}/bin/terp -r {1} -h {2} -o sum,pra,nist,html,param").format(path_to_terp,file1,file2)
    cmd=str("{0}/bin/terp -r {1} -h {2} -o html").format(path_to_terp,file1,file2)
    proc=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    output, errors=proc.communicate()
    if errors:
        print("Error in executing terp, terminating")
        shutil.rmtree(tmpdir)
        sys.exit()  
    else:
        shutil.rmtree(tmpdir) 
        terp_html=os.path.join(dir_path,"terp.html")
        soup = BeautifulSoup(codecs.open(terp_html,"r",encoding="utf-8"), 'html.parser')
        terp_score=soup.find(id='terpscore').string
        return({"terp_score":terp_score})
    
    
    
    
    
if __name__ == '__main__':
    sent1="ablaze car."
    sent2="car on fire"
    path_to_terp="/Users/amita/software/terp-master"
    res=score_terp(sent1,sent2,path_to_terp)