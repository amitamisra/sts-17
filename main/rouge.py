#!/usr/bin/env python2
# coding: utf8

'''
rouge similarity
@author: amita
'''

import os
import shutil
import tempfile
import codecs, logging



from pyrouge import Rouge155
log = logging.getLogger(__name__)
log.disabled =True

def writeTextFile(Filename, Lines):
    f = codecs.open(Filename, "w", encoding="utf-8")
    f.writelines(Lines)
    f.close()


def rouge(stringa, stringb):
    
    tmp = tempfile.mkdtemp()
    newrow = {}
    r = Rouge155()
    log = logging.getLogger(__name__)
    log.disabled =True
    count = 0
    dirname_sys = os.path.join(tmp, "rougue/System/")
    dirname_mod = os.path.join(tmp, "rougue/Model/")
    if not os.path.exists(dirname_sys):
        os.makedirs(dirname_sys)
    if not os.path.exists(dirname_mod):
        os.makedirs(dirname_mod)
    Filename = os.path.join(dirname_sys, "string_." + str(count) + ".txt")
    LinesA = list()
    LinesA.append(stringa)
    writeTextFile(Filename, LinesA)
    LinesB = list()
    LinesB.append(stringb)
    Filename = os.path.join(dirname_mod, "string_.A." + str(count) + ".txt")
    writeTextFile(Filename, LinesB)
    r.system_dir = dirname_sys
    r.model_dir = dirname_mod
    r.system_filename_pattern = 'string_.(\d+).txt'
    r.model_filename_pattern = 'string_.[A-Z].#ID#.txt'
    output = r.convert_and_evaluate()
    output_dict = r.output_to_dict(output)
    newrow["rouge_1_f_score"] = output_dict["rouge_1_f_score"]
    newrow["rouge_2_f_score"] = output_dict["rouge_2_f_score"]
    newrow["rouge_3_f_score"] = output_dict["rouge_3_f_score"]
    newrow["rouge_4_f_score"] = output_dict["rouge_4_f_score"]
    newrow["rouge_l_f_score"] = output_dict["rouge_l_f_score"]
    newrow["rouge_s*_f_score"] = output_dict["rouge_s*_f_score"]
    newrow["rouge_su*_f_score"] = output_dict["rouge_su*_f_score"]
    newrow["rouge_w_1.2_f_score"] = output_dict["rouge_w_1.2_f_score"]
    shutil.rmtree(tmp)
    return newrow

