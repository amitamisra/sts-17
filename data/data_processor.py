# Process sts-data-raw files into CSV with all file information
import os, re
from collections import defaultdict
from sts_17.data import data_utils, data_config


def aggregate_year_data(relevant_dir):
    year_data = []
    files = defaultdict(list)
    filenames, filepath = data_utils.get_raw_data_directory_contents(relevant_dir)

    for filename in filenames:
        if ".gs." in filename and filename.endswith(".txt") \
                and not filename.endswith("ALL.txt"):
            files["gs"].append(filename)
        elif ".input." in filename and filename.endswith(".txt"):
            files["input"].append(filename)
    data_and_labels = zip(files["input"], files["gs"])

    for data_file,label_file in data_and_labels:
        year = re.findall(r"STS(.*)-en", relevant_dir)[0]
        if re.findall(r"\.gs\.(.*)\.txt", label_file):
            domain = re.findall(r"\.gs\.(.*)\.txt", label_file)[0]
        else:
            domain = "[none]"
        data = data_utils.read_file_lines(os.path.join(filepath, data_file))
        labels = data_utils.read_file_lines(os.path.join(filepath, label_file))
        for i in range(len(labels)):
            print(relevant_dir, data_file, data[i])
            data[i] = re.sub("\t+", "\t", data[i])
            if data_file=="STS.input.belief.txt" and relevant_dir=="STS2015-en-trial":
                data[i] = re.sub("  +", "\t", data[i])
            elif relevant_dir=="STS2016-en-test":
                text_1 = data[i].split("\t")[0]
                text_2 = data[i].split("\t")[1]
                label = labels[i].rstrip()
            elif relevant_dir=="STS2012-en-trial":
                _, text_1, text_2 = data[i].rstrip().split("\t")
                _, label = labels[i].rstrip().split("\t")
            # files in 2016 also contain links to the text source
            else:
                text_1, text_2 = data[i].rstrip().split("\t")
                label = labels[i].rstrip()
            # some files in 2015 and 2016 have no label
            if len(label) >= 1:
                year_data.append([year, domain, label, text_1, text_2])
    return year_data


def process_data(start_year, end_year):
    raw_data_dirs,_ = data_utils.get_raw_data_directory_contents("")
    relevant_years = [str(year) for year in range(start_year, end_year+1)]
    relevant_dirs = [d for d in raw_data_dirs if d.split("-")[0][3:] in relevant_years]
    output_data = []
    for relevant_dir in relevant_dirs:
        output_data += aggregate_year_data(relevant_dir)
    return output_data


def main():
    train_data = process_data(2012, 2015)
    test_data = process_data(2016, 2016)

    header = ["year", "domain", "label", "text_1", "text_2"]
    data_utils.write_csv(data_config.train_2012_2015, train_data, header)
    data_utils.write_csv(data_config.test_2016, test_data, header)

if __name__=='__main__':
    main()