This README describes the data collection for the STS task.

When running on a new system, change the "MAIN_DIR" parameters in data_config.py to reflect the correct project path.

The sts-data-raw contains raw data files supplied for each STS task from years 2012-1207.
The sts-data-processed provides a single train file for data from 2012-2015, and a single test file for data from 2016.

Since not all pairs were used for testing, some pairs do not contain a "label" field.
Also, the labeling scheme has changed over the years. Some years use a float label, while others use an int label (all 0-5).
Finally, year 2016 introduces the "source" for each text, which is not currently included in the sts-data-processed versions.