# Utils for STS data processing
from sts_17.data import data_config
import os
import csv


def get_raw_data_directory_contents(path_from_raw):
    path = data_config.DATA_RAW_DIR+path_from_raw
    return os.listdir(path), path


def read_csv(filename):
    with open(filename, 'r', encoding="utf-8") as csvfile:
        reader = csv.DictReader(csvfile)
        fieldnames = reader.fieldnames
        return list(reader), fieldnames


def read_file_lines(filename):
    reader = open(filename, 'r', encoding="utf-8")
    lines = reader.readlines()
    return lines


def write_csv(filename, rows, header_fields=None):
    with open(filename, 'w', encoding="utf-8") as csvfile:
        writer = csv.writer(csvfile)
        if header_fields:
            writer.writerow(header_fields)
        for row in rows:
            writer.writerow(row)