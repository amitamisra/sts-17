/*
 sample test class to test cornlp server, the lib folder contains all the required jars. 
 ADD ALL THE JARS TO THE CLASSPATH
 stanfordcorenlp,
 stanford-corenlp-full-2015-12-09
stanford-postagger-2015-12-09
jcommander-1.58.jar
 author:amita
 */
package annotation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;


public class testSimpleCoreNLP_3_5_2 {
	protected SimpleCoreNLP_3_5_2options opts = new SimpleCoreNLP_3_5_2options();
	protected JCommander jc = null;
	
	
	/**
	 * Run the program based on the command-line options in {@link FacetSimilarityBootstrapOptions}.
	 * 
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void go(String[] args) throws IOException, InterruptedException {
		jc = new JCommander(opts);
		try {
			jc.parse(args);
		} catch (ParameterException e) {
			System.out.printf("Error processing command line arguments: %s\n", 
					e.getMessage());
			jc.usage();
			System.exit(-1);
		}
		
		String posmodel=opts.getposmodel();
		SimpleCoreNLP_3_5_2 nlpobj =new SimpleCoreNLP_3_5_2(posmodel);
		
		String test1 = "Most Christians dont try to do that to gay people either.Sure they do. A majority of self-identified Christians in THIS country oppose civil marriage for gay people. In Spain, for example, they don't. A huge majority of self-identified \"evangelical Christians\" oppose gay marriage, civil unions, gay adoption, anti-discrimination laws, the Lawrence decision to decriminalize same-sex sex, etc., etc., etc. As I recall, that predeliction knows no national borders.Oh, I know, they're not trying to ban really important things.... y a w nBut they're not trying to ban it.";
		//String test1="Notice not one of the common sense gun control laws gun hater pilot is demanding is to punish dangerous criminals by keeping them in prison to keep guns away from them!"; 
		ArrayList<HashMap<String, LinkedList<Object>>> hmaplist1 =nlpobj.parsed_text(test1);
		 for (HashMap<String, LinkedList<Object>> hmap1 : hmaplist1)
		 {
			 for (Entry<String, LinkedList<Object>> entry : hmap1.entrySet()) {
			    String key = entry.getKey();
			    Object value = entry.getValue();
			    System.out.print(key);
			    System.out.println(value);
			}
			 
		 }
	}
	public static void main(String[] args)throws Exception  {
		    new testSimpleCoreNLP_3_5_2().go(args);
		    
	}

}
/* 
 EXPECTED OUTPUT
deps_cc[[nsubj, 15, 0], [neg, 2, 1], [nsubj, 14, 2], [case, 12, 3], [det, 12, 4], [amod, 12, 5], [compound, 12, 6], [compound, 12, 7], [compound, 12, 8], [compound, 12, 9], [compound, 12, 10], [compound, 12, 11], [nmod:of, 2, 12], [aux, 14, 13], [acl:relcl, 0, 14], [mark, 17, 16], [xcomp, 15, 17], [amod, 19, 18], [dobj, 17, 19], [mark, 21, 20], [advcl, 17, 21], [dobj, 21, 22], [case, 24, 23], [nmod:in, 21, 24], [mark, 26, 25], [advcl, 21, 26], [dobj, 26, 27], [advmod, 26, 28], [case, 30, 29], [nmod:from, 26, 30], [punct, 15, 31], [root, -1, 15]]
pos[NNP, RB, CD, IN, DT, JJ, NN, NN, NN, NNS, NN, NN, NN, VBZ, VBG, VBZ, TO, VB, JJ, NNS, IN, VBG, PRP, IN, NN, TO, VB, NNS, RB, IN, PRP, .]
lemmas[Notice, not, one, of, the, common, sense, gun, control, law, gun, hater, pilot, be, demand, be, to, punish, dangerous, criminal, by, keep, they, in, prison, to, keep, gun, away, from, they, !]
tokens[Notice, not, one, of, the, common, sense, gun, control, laws, gun, hater, pilot, is, demanding, is, to, punish, dangerous, criminals, by, keeping, them, in, prison, to, keep, guns, away, from, them, !]
parse[(ROOT (S (NP (NP (NNP Notice)) (SBAR (S (NP (NP (RB not) (CD one)) (PP (IN of) (NP (DT the) (JJ common) (NN sense) (NN gun) (NN control) (NNS laws) (NN gun) (NN hater) (NN pilot)))) (VP (VBZ is) (VP (VBG demanding)))))) (VP (VBZ is) (S (VP (TO to) (VP (VB punish) (NP (JJ dangerous) (NNS criminals)) (PP (IN by) (S (VP (VBG keeping) (NP (PRP them)) (PP (IN in) (NP (NN prison))) (S (VP (TO to) (VP (VB keep) (NP (NNS guns)) (ADVP (RB away)) (PP (IN from) (NP (PRP them))))))))))))) (. !)))]
char_offset[[0, 6], [7, 10], [11, 14], [15, 17], [18, 21], [22, 28], [29, 34], [35, 38], [39, 46], [47, 51], [52, 55], [56, 61], [62, 67], [68, 70], [71, 80], [81, 83], [84, 86], [87, 93], [94, 103], [104, 113], [114, 116], [117, 124], [125, 129], [130, 132], [133, 139], [140, 142], [143, 147], [148, 152], [153, 157], [158, 162], [163, 167], [167, 168]]

 */

