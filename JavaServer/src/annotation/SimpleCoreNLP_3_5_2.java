package annotation;

import java.io.*;
import java.util.*;

import edu.stanford.nlp.io.*;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.util.*;
import edu.stanford.nlp.simple.*;
import py4j.GatewayServer;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentenceIndexAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.IndexAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.CharacterOffsetBeginAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.CharacterOffsetEndAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;

import java.util.HashMap;



public class SimpleCoreNLP_3_5_2{
	private StanfordCoreNLP pipeline;
	
	public SimpleCoreNLP_3_5_2(String posmodel_loc)
	{
		Properties props = new Properties();
		   // props.put("annotators", "tokenize, ssplit, pos, lemma, parse"); DONE FOR NAACL
	   props.put("pos.model", posmodel_loc);
	   props.put("annotators", "tokenize, ssplit, pos, lemma, parse");
	   this.pipeline = new StanfordCoreNLP(props);
	}
	public StanfordCoreNLP getpipeline()
	{
		return this.pipeline;
	}
	
	// modified to return token index starting from 0 instead of 1.
	//This behaviour is different from normalcorenlp
	public LinkedList<Object> getdeplist(SemanticGraph dependencies )
	{    LinkedList<Object> all_dep_lists =new LinkedList<Object>();
		List <SemanticGraphEdge> edge_set1 = dependencies.edgeListSorted();
		for(SemanticGraphEdge edge : edge_set1)
	      {    ArrayList<Object> deplist= new ArrayList<Object>();
	          IndexedWord dep = edge.getDependent();
	          IndexedWord gov = edge.getGovernor();
	      GrammaticalRelation relation = edge.getRelation();
	      deplist.add( relation.toString());
	      deplist.add(gov.index()-1);
	      deplist.add(dep.index()-1);
	      all_dep_lists.add(deplist);
		}
		
		 ArrayList<Object> deplist= new ArrayList<Object>();
	      IndexedWord root= dependencies.getFirstRoot();
	      int rootindex= root.index();
	      String relation = new String("root");
	      deplist.add( relation.toString());
	      deplist.add(-1);
	      deplist.add(rootindex -1);
	    
	      all_dep_lists.add(deplist);
	      return  all_dep_lists;
	}
	public ArrayList<HashMap<String,LinkedList<Object>>> parsed_text(String text)
	{   Tree tree;
		Annotation annotation = this.pipeline.process(text);
        List<CoreMap> sentences = annotation.get(SentencesAnnotation.class);
        ArrayList<HashMap <String,LinkedList<Object>>> hm_list =  new ArrayList <HashMap<String,LinkedList<Object>>>();
         
        for(CoreMap sentence: sentences) { 
            List<CoreLabel> tokens = sentence.get(TokensAnnotation.class);
            HashMap <String,LinkedList<Object>> hm =  new HashMap <String,LinkedList<Object>>();
             LinkedList<Object> tokenList = new LinkedList<Object>();
         	 LinkedList<Object> lemmaList = new LinkedList<Object>();
         	 LinkedList<Object> posList = new LinkedList<Object>();
         	 LinkedList<Object> parsetreeList = new LinkedList<Object>();
         	 LinkedList<Object> charoffsetList = new LinkedList<Object>();
         	 
             for (CoreLabel token: tokens) { 
            	   ArrayList<Object>  offsetList= new ArrayList<Object>();
                   String word = token.get(TextAnnotation.class);
                   Integer index= token.get(IndexAnnotation.class);
                   String pos=token.getString(PartOfSpeechAnnotation.class);
                   String lemma=token.getString(LemmaAnnotation.class);
                   Integer offsetbegin= token.get(CharacterOffsetBeginAnnotation.class);
                   Integer offsetend=token.get(CharacterOffsetEndAnnotation.class);
                   offsetList.add(offsetbegin);
                   offsetList.add(offsetend);
                   charoffsetList.add(offsetList);
                   tokenList.add(word);
                   lemmaList.add(lemma);
                   posList.add(pos);
                   
                 
             }
             tree = sentence.get(TreeAnnotation.class);
             parsetreeList.add(tree.toString());

             SemanticGraph depgraph = sentence.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
             //SemanticGraph basicdependencies= sentence.get(SemanticGraphCoreAnnotations.BasicDependenciesAnnotation.class);
             hm.put("tokens",  tokenList);
             hm.put("pos",posList);
             hm.put("lemmas", lemmaList);
             hm.put("char_offset", charoffsetList);
             hm.put("parse", parsetreeList);
             hm.put("deps_cc",getdeplist(depgraph));
             hm_list.add(hm);
             
         }
         
        
        
         return hm_list;
		
	}
	public static void main(String[] args) {
	    // app is now the gateway.entry_point
		String pos_model="lib/stanford-postagger-2015-12-09/models/english-bidirectional-distsim.tagger";
	    GatewayServer server = new GatewayServer(new SimpleCoreNLP_3_5_2(pos_model),25335);
	    server.start();
	    System.out.println("Gateway Server Started");
	  }
	}

