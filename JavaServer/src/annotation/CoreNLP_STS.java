package annotation;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

//import org.apache.log4j.Logger;

import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import java.io.FileReader;
public class CoreNLP_STS {

	//static final Logger LOG = Logger.getLogger(FacetSimilarityBootstrap.class);
	protected JCommander jc = null;
	protected CoreNLP_STS_options opts = new CoreNLP_STS_options();
	
	public static void main(String[] args) throws Exception{
		new CoreNLP_STS().go(args);
	}
	
	
	/**
	 * Run the program based on the command-line options in {@link CoreNLP_STS_options.java}.
	 * 
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void go(String[] args) throws IOException, InterruptedException {
		jc = new JCommander(opts);
		try {
			jc.parse(args);
		} catch (ParameterException e) {
			System.out.printf("Error processing command line arguments: %s\n", 
					e.getMessage());
			jc.usage();
			System.exit(-1);
		}
		
		writeAnnotation();
	}
	
	/**
	 * Controls the main flow. 
	 * 
	 * Iterates over the input json file and collects annotations then
	 * outputs those annotations to a json file. 
	 * 
	 * @throws InterruptedException
	 */
	private void writeAnnotation() throws InterruptedException {
		
	String	datacolumn1=opts.getDataColumn1();
	String datacolumn2=opts.getDataColumn2();
	String inputFile=opts.getInputFile();
	String outputFile=opts.getOutputFile();
	LinkedList<String> FieldNameAnotation=new LinkedList<String> ();
	FieldNameAnotation.add(datacolumn1);
	FieldNameAnotation.add(datacolumn2);	
	
	JSONArray Json_Rows_annotations;
	 try {
		    String posmodel=opts.getposmodel();
			SimpleCoreNLP_3_5_2 corenlp_object =new SimpleCoreNLP_3_5_2(posmodel);
			JSONParser parser = new JSONParser(); 
			JSONArray AllRows;
			System.out.println(inputFile);
		    AllRows = (JSONArray) parser.parse(new FileReader(inputFile));	
			Json_Rows_annotations = this.GetCoreNLpAnnotations(AllRows,FieldNameAnotation,corenlp_object);
			JsonArray_to_Txt(Json_Rows_annotations, outputFile);
			} 
		catch (Exception e) 
		  {
			e.printStackTrace();
			System.exit(0);
		  }
		
	}
   
	
	/**
	 * Use the SimpleCoreNLp class to create a corenlp object
	 * iterate over rows as JsonArray
	 * FieldNameAnnotation contains a list of column names that contains text to be parsed
	 * 
	 */
	private  JSONArray GetCoreNLpAnnotations(JSONArray Allrows, LinkedList<String>FieldNameAnotation,SimpleCoreNLP_3_5_2 corenlp_object) throws IOException
	{
		JSONArray Json_rows =new JSONArray();  

		try 
		{
			for(Object mapobj : Allrows)
			{
				JSONObject  extend_row=new JSONObject();  
				JSONObject entry = (JSONObject) mapobj;
				Set<String> keyset=entry.keySet();
				for (String key:keyset )
				{
					if ((FieldNameAnotation).contains(key.toString()))
					{
						ArrayList<HashMap<String, LinkedList<Object>>> hmaplist=corenlp_object.parsed_text(entry.get(key).toString());
						extend_row.put(key+"_core_nlp_annotations", hmaplist);
					}
				}
				extend_row.putAll(entry);
		        Json_rows.add(extend_row);
			}
	 }catch (Exception e) {
	    System.err.println(e);
	    System.exit(0);
	}
	return Json_rows;
	}
	
	
	
	public void JsonArray_to_Txt(ArrayList<LinkedHashMap<String, String>> json_rows,String outfile) {
		try {
			Writer file = new BufferedWriter(new OutputStreamWriter(
				    new FileOutputStream(outfile), "UTF-8"));
        	String jsonString = JSONValue.toJSONString(json_rows);
            file.write(jsonString);
            file.flush();
            file.close();
 
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        } 
	}


}
